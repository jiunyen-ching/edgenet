import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras import layers
from tensorflow.keras.layers import Concatenate, Conv2D, Conv3D, Add, MaxPooling3D, BatchNormalization, UpSampling3D, Conv3DTranspose
from tensorflow.keras.layers import Reshape, ReLU, Softmax

class BasicDDR3D(layers.Layer):
    def __init__(self, c, k=3, dilation_rate=1, stride=1, residual=True):
        super(BasicDDR3D, self).__init__()
        self.d = dilation_rate
        self.p = k // 2 * self.d
        # p = (d * (k - 1) + 1) // 2
        self.s = stride
        # print("k:{}, d:{}, p:{}".format(k, d, p))
        self.conv_1x1xk = layers.Conv3D(c, (1, 1, k), strides=(1, 1, self.s), padding='valid', use_bias=True, dilation_rate=(1, 1, self.d))
        self.conv_1xkx1 = layers.Conv3D(c, (1, k, 1), strides=(1, self.s, 1), padding='valid', use_bias=True, dilation_rate=(1, self.d, 1))
        self.conv_kx1x1 = layers.Conv3D(c, (k, 1, 1), strides=(self.s, 1, 1), padding='valid', use_bias=True, dilation_rate=(self.d, 1, 1))
        self.residual = residual
        self.act = layers.ReLU()

    def call(self, x):
        y = tf.pad(x, [[0,0], [0,0], [0,0], [self.p,self.p], [0,0]], 'CONSTANT')
        y = self.conv_1x1xk(y)
        y = self.act(y)
        y = tf.pad(y, [[0,0], [0,0], [self.p,self.p], [0,0], [0,0]], 'CONSTANT')
        y = self.conv_1xkx1(y)
        y = self.act(y)
        y = tf.pad(y, [[0,0], [self.p,self.p], [0,0], [0,0], [0,0]], 'CONSTANT')
        y = self.conv_kx1x1(y)
        y = self.act(y + x) # if self.residual else F.relu(y, inplace=True)
        return y

class BottleneckDDR3D(layers.Layer):
    def __init__(self, c, k=3, dilation_rate=1, stride=1, residual=True):
        super(BottleneckDDR3D, self).__init__()
        self.d = dilation_rate
        self.p = k // 2 * self.d
        # p = (d * (k - 1) + 1) // 2
        self.s = stride
        # print("k:{}, d:{}, p:{}".format(k, d, p))
        self.conv_in = layers.Conv3D(c, kernel_size=1, use_bias=False)
        self.conv_1x1xk = layers.Conv3D(c, (1, 1, k), strides=(1, 1, self.s), padding='valid', use_bias=True, dilation_rate=(1, 1, self.d))
        self.conv_1xkx1 = layers.Conv3D(c, (1, k, 1), strides=(1, self.s, 1), padding='valid', use_bias=True, dilation_rate=(1, self.d, 1))
        self.conv_kx1x1 = layers.Conv3D(c, (k, 1, 1), strides=(self.s, 1, 1), padding='valid', use_bias=True, dilation_rate=(self.d, 1, 1))
        self.conv_out = layers.Conv3D(c, kernel_size=1, use_bias=False)
        self.residual = residual
        self.act = layers.ReLU()

    def call(self, x):
        y0 = self.conv_in(x)
        y0 = self.act(y0)

        y1 = tf.pad(y0, [[0,0], [0,0], [0,0], [self.p,self.p], [0,0]], 'CONSTANT')
        y1 = self.conv_1x1xk(y1)
        y1 = self.act(y1)

        y2 = tf.pad(y1, [[0,0], [0,0], [self.p,self.p], [0,0], [0,0]], 'CONSTANT')
        y2 = self.conv_1xkx1(y2) + y1
        y2 = self.act(y2)

        y3 = tf.pad(y2, [[0,0], [self.p,self.p], [0,0], [0,0], [0,0]], 'CONSTANT')
        y3 = self.conv_kx1x1(y3) + y2 + y1
        y3 = self.act(y3) # if self.residual else F.relu(y, inplace=True)

        y = self.conv_out(y3)
        y = self.act(y + x)
        return y

class BasicDDR_UNET(Model):
    def __init__(self):
        super(BasicDDR_UNET, self).__init__()
        self.conv1 = layers.Conv3D(8, 3, padding='same')
        self.resblock1 = BasicDDR3D(8)

        self.conv2 = layers.Conv3D(16, 3, padding='same')
        self.resblock2 = BasicDDR3D(16)

        self.conv3 = layers.Conv3D(32, 3, padding='same')
        self.resblock3 = BasicDDR3D(32)

        self.conv4 = layers.Conv3D(64, 3, padding='same')
        self.resblock4 = BasicDDR3D(64, dilation_rate=2)

        self.conv5 = layers.Conv3D(128, 3, padding='same')
        self.resblock5 = BasicDDR3D(128, dilation_rate=2)
        self.upconv5 = layers.Conv3DTranspose(64, (2, 2, 2), strides=(2,2,2))

        self.conv6 = layers.Conv3D(64, 3, padding='same')
        self.resblock6 = BasicDDR3D(64, dilation_rate=2)
        self.upconv6 = layers.Conv3DTranspose(32, (2, 2, 2), strides=(2,2,2))

        self.conv7 = layers.Conv3D(32, 3, padding='same')
        self.resblock7 = BasicDDR3D(32)

        self.conv81 = layers.Conv3D(16, 1, padding='same')
        self.conv82 = layers.Conv3D(16, 1, padding='same')
        self.conv9 = layers.Conv3D(12, 1, padding='same', activation='softmax')

        self.pool = layers.MaxPooling3D((2, 2, 2), strides=(2, 2, 2))
        self.concat = layers.Concatenate()

    def call(self, input_tensor, training=False):

        x = self.conv1(input_tensor)
        x = self.resblock1(x)

        x = self.pool(x)
        x = self.conv2(x)
        x = self.resblock2(x)

        x1 = self.pool(x)
        x = self.conv3(x1)
        x2 = self.resblock3(x)

        x = self.pool(x2)
        x = self.conv4(x)
        x = self.resblock4(x)
        x3 = self.resblock4(x)

        x = self.pool(x)
        x = self.conv5(x)
        x = self.resblock5(x)
        x = self.resblock5(x)
        x = self.upconv5(x)

        # x = self.concat((x,x3))
        x = self.concat([x,x3])
        x = self.conv6(x)
        x = self.resblock6(x)
        x = self.resblock6(x)
        x = self.upconv6(x)

        # x = self.concat((x,x2))
        x = self.concat([x,x2])
        x = self.conv7(x)
        x = self.resblock7(x)
        # x = self.concat((x,x1))
        x = self.concat([x,x1])
        x = self.conv81(x)
        x = self.conv82(x)
        x = self.conv9(x)

        return x

class BottleneckDDR_UNET(Model):
    def __init__(self):
        super(BottleneckDDR_UNET, self).__init__()
        self.conv1 = layers.Conv3D(8, 3, padding='same')
        self.resblock1 = BottleNeckDDR3D(8)

        self.conv2 = layers.Conv3D(16, 3, padding='same')
        self.resblock2 = BottleNeckDDR3D(16)

        self.conv3 = layers.Conv3D(32, 3, padding='same')
        self.resblock3 = BottleNeckDDR3D(32)

        self.conv4 = layers.Conv3D(64, 3, padding='same')
        self.resblock4 = BottleNeckDDR3D(64, dilation_rate=2)

        self.conv5 = layers.Conv3D(128, 3, padding='same')
        self.resblock5 = BottleNeckDDR3D(128, dilation_rate=2)
        self.upconv5 = layers.Conv3DTranspose(64, (2, 2, 2), strides=(2,2,2))

        self.conv6 = layers.Conv3D(64, 3, padding='same')
        self.resblock6 = BottleNeckDDR3D(64, dilation_rate=2)
        self.upconv6 = layers.Conv3DTranspose(32, (2, 2, 2), strides=(2,2,2))

        self.conv7 = layers.Conv3D(32, 3, padding='same')
        self.resblock7 = BottleNeckDDR3D(32)

        self.conv81 = layers.Conv3D(16, 1, padding='same')
        self.conv82 = layers.Conv3D(16, 1, padding='same')
        self.conv9 = layers.Conv3D(12, 1, padding='same', activation='softmax')

        self.pool = layers.MaxPooling3D((2, 2, 2), strides=(2, 2, 2))
        self.concat = layers.Concatenate()

    def call(self, input_tensor, training=False):

        x = self.conv1(input_tensor)
        x = self.resblock1(x)

        x = self.pool(x)
        x = self.conv2(x)
        x = self.resblock2(x)

        x1 = self.pool(x)
        x = self.conv3(x1)
        x2 = self.resblock3(x)

        x = self.pool(x2)
        x = self.conv4(x)
        x = self.resblock4(x)
        x3 = self.resblock4(x)

        x = self.pool(x)
        x = self.conv5(x)
        x = self.resblock5(x)
        x = self.resblock5(x)
        x = self.upconv5(x)

        # x = self.concat((x,x3))
        x = self.concat([x,x3])
        x = self.conv6(x)
        x = self.resblock6(x)
        x = self.resblock6(x)
        x = self.upconv6(x)

        # x = self.concat((x,x2))
        x = self.concat([x,x2])
        x = self.conv7(x)
        x = self.resblock7(x)
        # x = self.concat((x,x1))
        x = self.concat([x,x1])
        x = self.conv81(x)
        x = self.conv82(x)
        x = self.conv9(x)

        return x

class RESNETBlock(layers.Layer):
    def __init__(self, out_channels, kernel_size=3, dilation_rate=1):
        super(RESNETBlock, self).__init__()
        self.conv = layers.Conv3D(out_channels, kernel_size, padding='same', dilation_rate=dilation_rate)
        self.bn = layers.BatchNormalization()
        self.act = layers.ReLU()

    def call(self, input_tensor, training=False):
        x = self.bn(input_tensor, training=training)
        x = self.act(x)
        x = self.conv(x)
        x = self.bn(x, training=training)
        x = self.act(x)
        x = self.conv(x)
        x = x + input_tensor
        return x

class Bottleneck2D_Block(layers.Layer):
    def __init__(self, out_channels):
        super(Bottleneck2D_Block, self).__init__()
        self.conv_in = layers.Conv2D(out_channels // 2, 1, data_format='channels_first')
        self.conv = layers.Conv2D(out_channels // 2, 3, padding='same', data_format='channels_first')
        self.conv_out = layers.Conv2D(out_channels, 1, data_format='channels_first')
        self.add = layers.Add()
        self.act = layers.ReLU()

    def call(self, input_tensor, training=False):
        x = self.conv_in(input_tensor)
        x = self.act(x)
        x = self.conv(x)
        x = self.act(x)
        x = self.conv_out(x)
        x = self.add([x,input_tensor])
        x = self.act(x)

        return x

class Bottleneck2D_CNN(layers.Layer):
    def __init__(self, out_channels):
        super(Bottleneck2D_CNN, self).__init__()
        self.conv = layers.Conv2D(out_channels, kernel_size=1, padding='same', data_format='channels_first')
        self.bottleneck_2d_1 = Bottleneck2D_Block(out_channels)
        self.bottleneck_2d_2 = Bottleneck2D_Block(out_channels)

    def call(self, input_tensor, training=False):
        x = self.conv(input_tensor)
        x = self.bottleneck_2d_1(x)
        x = self.bottleneck_2d_2(x)

        return x

class RES_UNET(Model):
    def __init__(self, use_rgb=False, fuse_early_features=False):
        super(RES_UNET, self).__init__()

        self._2dcnn = Bottleneck2D_CNN(8)
        self.project = ProjectionLayer()

        self.conv1 = layers.Conv3D(8, 3, padding='same')
        self.resblock1 = RESNETBlock(8)

        self.conv2 = layers.Conv3D(16, 3, padding='same')
        self.resblock2 = RESNETBlock(16)

        self.conv3 = layers.Conv3D(32, 3, padding='same')
        self.resblock3 = RESNETBlock(32)

        self.conv4 = layers.Conv3D(64, 3, padding='same')
        self.resblock4 = RESNETBlock(64, dilation_rate=2)

        self.conv5 = layers.Conv3D(128, 3, padding='same')
        self.resblock5 = RESNETBlock(128, dilation_rate=2)
        self.upconv5 = layers.Conv3DTranspose(64, (2, 2, 2), strides=(2,2,2))

        self.conv6 = layers.Conv3D(64, 3, padding='same')
        self.resblock6 = RESNETBlock(64, dilation_rate=2)
        self.upconv6 = layers.Conv3DTranspose(32, (2, 2, 2), strides=(2,2,2))

        self.conv7 = layers.Conv3D(32, 3, padding='same')
        self.resblock7 = RESNETBlock(32)

        self.conv8_1 = layers.Conv3D(16, 1, padding='same')
        self.conv8_2 = layers.Conv3D(16, 1, padding='same')
        self.conv9 = layers.Conv3D(12, 1, padding='same', activation='softmax')

        self.pool = layers.MaxPooling3D((2, 2, 2), strides=(2, 2, 2))
        self.concat = layers.Concatenate()
        self.add = layers.Add()

        self.use_rgb = use_rgb
        self.fuse_early_features = fuse_early_features

    def call(self, input_tensor, training=False):

        if self.use_rgb:
            rgb_2d = self._2dcnn(input_tensor[1])
            rgb_3d = self.project((rgb_2d, input_tensor[2]))

            rgb_3d = self.conv1(rgb_3d)
            rgb_3d = self.resblock1(rgb_3d, training=training)

            rgb_3d = self.pool(rgb_3d, training=training)
            rgb_3d = self.conv2(rgb_3d)
            rgb_3d = self.resblock2(rgb_3d, training=training)

        x0 = self.conv1(input_tensor)
        x0 = self.resblock1(x0, training=training)

        x1 = self.pool(x0, training=training)
        x1 = self.conv2(x1)
        x1 = self.resblock2(x1, training=training)

        if self.use_rgb:
            x1 = self.concat([rgb_3d, x1])

        x2_0 = self.pool(x1, training=training)
        x2_0 = self.conv3(x2_0)
        x2_1 = self.resblock3(x2_0, training=training)

        x3 = self.pool(x2_1, training=training)
        x3 = self.conv4(x3)
        x3 = self.resblock4(x3, training=training)
        x3 = self.resblock4(x3, training=training)

        x4 = self.pool(x3, training=training)
        x4 = self.conv5(x4)
        x4 = self.resblock5(x4, training=training)
        x4 = self.resblock5(x4, training=training)
        x4 = self.upconv5(x4)

        x5 = self.concat([x3,x4])
        x5 = self.conv6(x5)
        x5 = self.resblock6(x5, training=training)
        x5 = self.resblock6(x5, training=training)
        x5 = self.upconv6(x5)

        x6 = self.concat([x2_1,x5])
        x6 = self.conv7(x6)
        x6 = self.resblock7(x6, training=training)

        if self.fuse_early_features:
            x6 = self.concat([x2_0,x6, self.pool(x1, training=training)])
        else:
            x6 = self.concat([x2_0,x6])
        x6 = self.conv8_1(x6)
        x6 = self.conv8_2(x6)
        x6 = self.conv9(x6)

        return x6

class ProjectionLayer(layers.Layer):
    def __init__(self):
        super(ProjectionLayer, self).__init__()

    def call(self, inputs):
        updates = inputs[0]
        mapping = inputs[1]

        bs, ch, h, w = tf.shape(updates)

        updates = tf.reshape(updates, (-1, ch, h * w))
        mapping = tf.reshape(mapping, (-1, 1, h * w))
        mapping = tf.tile(mapping, (1, ch, 1))
        mapping = tf.expand_dims(mapping, axis=-1)

        ch_idx = tf.range(ch)
        ch_idx = tf.expand_dims(ch_idx, axis=1)
        ch_idx = tf.tile(ch_idx, (1, h * w))
        ch_idx = tf.expand_dims(ch_idx, axis=-1)
        ch_idx = tf.expand_dims(ch_idx, axis=0)
        ch_idx = tf.repeat(ch_idx, repeats=(bs), axis=0)

        indices = tf.concat((ch_idx, mapping), axis=-1)

        batch_idx = tf.range(bs)
        batch_idx = tf.expand_dims(batch_idx, axis=-1)
        batch_idx = tf.tile(batch_idx, (1, ch * h * w))
        batch_idx = tf.reshape(batch_idx,(-1, ch, h * w))
        batch_idx = tf.expand_dims(batch_idx, axis=-1)

        indices = tf.concat((batch_idx, indices), axis=-1)

        tensor = tf.zeros((bs, ch, 240*144*240), dtype=tf.float32)
        tensor = tf.tensor_scatter_nd_max(tensor, indices, updates)
        tensor = tf.reshape(tensor, (-1, ch, 240, 144, 240))
        tensor = tf.transpose(tensor, (0,2,3,4,1))

        return tensor

class RES_UNET_RGB(Model):
    def __init__(self):
        super(RES_UNET_RGB, self).__init__()
        self._2DCNN = Bottleneck2D_CNN(8)
        self.project = ProjectionLayer()
        self.conv1 = layers.Conv3D(8, kernel_size=3, padding='same')
        self.conv9 = layers.Conv3D(12, 1, padding='same', activation='softmax')

        self.pool = layers.MaxPooling3D((2, 2, 2), strides=(2, 2, 2))
        self.concat = layers.Concatenate()

    def call(self, input_tensor):
        rgb_2d = self._2DCNN(input_tensor[1])
        rgb_3d = self.project((rgb_2d, input_tensor[2]))
        x = self.conv1(input_tensor[0])
        x = self.concat([x,rgb_3d])
        x = self.pool(x)
        x = self.pool(x)
        x = self.conv9(x)

        return x


def get_proj(updates, mapping):

    _, ch, h, w = updates.shape
    bs          = tf.shape(updates)[0]

    x, y, z = 240, 144, 240

    updates = tf.reshape(updates, (-1,ch,h*w))
    mapping = tf.reshape(mapping, (-1,1,h*w))
    mapping = tf.tile(mapping, (1,ch,1))
    mapping = tf.expand_dims(mapping, axis=-1)
    mapping = tf.cast(mapping, tf.int32)

    ch_idx = tf.range(ch)
    ch_idx = tf.expand_dims(ch_idx, axis=1)
    ch_idx = tf.tile(ch_idx, (1,h*w))
    ch_idx = tf.expand_dims(ch_idx, axis=-1)
    ch_idx = tf.expand_dims(ch_idx, axis=0)
    ch_idx = tf.repeat(ch_idx, repeats=(bs), axis=0)

    indices = tf.concat((ch_idx, mapping), axis=-1)

    batch_idx = tf.range(tf.shape(updates)[0])
    batch_idx = tf.expand_dims(batch_idx, axis=-1)
    batch_idx = tf.tile(batch_idx, (1,ch*h*w))
    batch_idx = tf.reshape(batch_idx, (-1,ch,h*w))
    batch_idx = tf.expand_dims(batch_idx, axis=-1)

    indices = tf.concat((batch_idx, indices), axis=-1)

    tensor = tf.zeros((bs,ch,x*y*z), dtype=tf.float32)
    tensor = tf.tensor_scatter_nd_max(tensor, indices, updates)
    tensor = tf.reshape(tensor, (-1,ch,x,y,z))
    tensor = tf.transpose(tensor, (0,2,3,4,1))

    return tensor

######### RESNET | DEPTH, COLOR, [DEPTH, EDGES] #########

def get_res_unet():

    # model = DDR_UNET()
    # model = BottleNeckDDR_UNET()
    model = RES_UNET(use_rgb=False, fuse_early_features=False)

    return model

def get_res_unet_proj():
    model = RES_UNET_RGB()
    return model

# def get_res_unet_proj():
#     input_color     = Input(shape=(3, 480, 640))
#     input_mapping   = Input(shape=(1, 480, 640))
#     # feat_color_2d   = get_2d_cnn(input_color)
#     feat_color_3d   = get_proj(input_color, input_mapping)
#     factor_4_color  = get_res_unet_input_branch(feat_color_3d)
#
#     input_tsdf      = Input(shape=(240, 144, 240, 1))
#     factor_4_tsdf   = get_res_unet_input_branch(input_tsdf)
#     factor_4        = concatenate([factor_4_tsdf, factor_4_color], axis=-1)
#     # factor_4        = Add()([factor_4_tsdf, factor_4_color])
#     fin_1, fin_2    = get_res_unet_backbone(factor_4)
#     model           = Model(inputs=[input_color, input_mapping, input_tsdf], outputs=[fin_1,fin_2])
#
#     return model

def get_network_by_name(name):

    # if name == 'R_UNET':        return get_res_unet(),          'depth'
    if name == 'R_UNET':        return get_res_unet()
    elif name == 'R_UNET_PROJ': return get_res_unet_proj()

def get_net_name_from_w(weights):
    networks = ['R_UNET', 'R_UNET_E', 'R_UNET_PROJ']

    for net in networks:
        if ((net+'_LR') == (weights[0:len(net)+3])) or ((net+'fine_LR') == (weights[0:len(net)+7])):
            return net

    print("Invalid weight file: ", weights)
    exit(-1)
