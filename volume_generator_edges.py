import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ["KERAS_BACKEND"]="tensorflow"
import sys

from lib_csscnet.file_utils import *
from lib_csscnet.metrics import *
from lib_csscnet.losses import *

import numpy as np # linear algebra
from keras.optimizers import SGD

from lib_csscnet.network import *

import h5py

segmentation_class_map = np.array([0, 1, 2, 3, 4, 11, 5, 6, 7, 8, 8, 10, 10, 10, 11, 11, 9, 8, 11, 11,
                                   11, 11, 11, 11, 11, 11, 11, 10, 10, 11, 8, 10, 11, 9, 11, 11, 11], dtype=np.int32)

##################################

full_sh = (240, 144, 240)
down_sh = (60, 36, 60)
qty = 6

class_names = ["ceil.", "floor", "wall ", "wind.", "chair", "bed  ", "sofa ", "table", "tvs  ", "furn.", "objs."]

# NYU
data_path = "/content/drive/My Drive/Dataset/csscnet_edges_preproc/NYUtest"

file_prefixes = get_file_prefixes_from_path(data_path, criteria='*.npz')


# Select some scenes
file_prefixes = [x for x in file_prefixes if os.path.basename(x) in ['NYU0959_0000', 'NYU0638_0000',
                                                                     'NYU1202_0000', 'NYU1337_0000',
                                                                     'NYU1314_0000', 'NYU0524_0000',
                                                                     'NYU1395_0000']]


#####################################
#
# Select the type of data
#
#####################################
# edges networks
datagen = preproc_generator(file_prefixes, batch_size=1, shuff=False, shape=(240, 144, 240), type="edges", vol=True)

# non edges networks
# datagen = preproc_generator(file_prefixes, batch_size=1, shuff=False, shape=(240, 144, 240), type="depth", vol=True)



#GROUND TRUTH#
for i,file_prefix in enumerate(file_prefixes):

    print("Generating volumes for ", file_prefix)

    x, y, vol = next(datagen)

    vox_tsdf = x[0][0]
    vox_edges = x[1][0]

    segmentation_label = np.argmax((y[0]>=1).astype(int), axis=-1)
    vox_weights = np.max(y[0]-1, axis=-1)

    vox_vol = vol[0]

    print(y[0].shape)
    print(segmentation_label.shape)

    vox_vol[vox_vol==0] = 10

    print("mygt")
    voxel_export("./blender/nyugt"+str(i)+".bin", segmentation_label, shape=(60, 36, 60))


#####################################
#
# Select the type of network
#
#####################################
# model = get_sscnet_edges()     #E-SSCNet
# model = get_sscnet()           #SSCNet
model = get_res_unet_edges()   #EdgeNet
# model = get_res_unet()         #U-SSCNET


model.compile(optimizer=SGD(lr=0.01, decay=0.005,  momentum=0.9),
              loss=weighted_categorical_crossentropy
              ,metrics=[comp_iou, seg_iou]
              )

#####################################
#
# Select WEIGHT FILE
#
#####################################
# model_name = os.path.join('./weights','UNET_E_LR0.01_DC0.0005_3212-0.70-0.72.hdf5')
# model_name = os.path.join('./weights','SSCNET_E_LR0.01_DC0.0005_8530-0.57-0.53.hdf5')
# model_name = os.path.join('./weights','SSCNET_LR0.01_DC0.0005_10029-0.56-0.53.hdf5')
# model_name = os.path.join('./weights','R_UNET_E_LR0.01_DC0.0005_4318-0.67-0.54.hdf5')
model_name = os.path.join('./weights','R_UNET_E_LR0.01_DC0.0005_4535-0.77-0.55.hdf5')
# model_name = os.path.join('./weights','R_UNET_LR0.01_DC0.0005_621-0.69-0.54.hdf5')

model.load_weights(model_name)


for i,file_prefix in enumerate(file_prefixes):

    print("Generating volumes for ", file_prefix)

    x, y, vol = next(datagen)
    pred = model.predict(x=x)
    weights = np.amax(y - 1,axis=-1, keepdims=False)
    flags = np.array((weights!=0),dtype='float32')
    y_true = np.argmax(y, axis=-1) * flags
    y_pred = np.argmax(pred, axis=-1) * flags

    voxel_export("./blender/nyu_R_UNET_E"+str(i)+".bin", y_pred, shape=(60, 36, 60))
    print("nyu_R_UNET_E_Pred")

    # voxel_export("./blender/nyu_R_UNET" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
    # print("nyu_R_UNET_Pred")

#    voxel_export("./blender/nyu_SSCNET_E" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
#    print("nyu_SSCNET_E_Pred")

#    voxel_export("./blender/nyu_SSCNET" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
#    print("nyu_SSCNET_Pred")

#    voxel_export("./blender/nyu_label" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
#    print("nyu_UNET_Pred")
