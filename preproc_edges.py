import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ["KERAS_BACKEND"]="tensorflow"
from lib_csscnet.file_utils import *
import numpy as np # linear algebra
import multiprocessing
import argparse
import time
import random
from pprint import pprint

# SUNCG
# DATASET = 'SUNCGtrain'
# DATASET = 'SUNCGtest_49700_49884'
# BASE_PATH = '/(...)/sscnet/data/depthbin'

# NYUv2
# DATASET = 'NYUtest'
# DATASET = 'NYUtrain'
# BASE_PATH = '/media/mmu/af326b7d-b9b7-4f2e-8630-ed1069f9f08d/depthbin_eval/depthbin/NYU'

# NYUCAD
# DATASET = 'NYUCADtest'
# DATASET = 'NYUCADtrain'
# BASE_PATH = '/media/mmu/af326b7d-b9b7-4f2e-8630-ed1069f9f08d/depthbin_eval/depthbin/NYUCAD'

# SUNCGGEN (author's re-render of SUNCG)
# DATASET = 'SUNCGGENtrain'
# DATASET = 'SUNCGGENtest'
# BASE_PATH = '/d02/data/csscnet/SUNCG'

DEST_PATH = '/home/mmu/Desktop/csscnet_preproc'

DEVICE = 1
THREADS = 4

# Globals
proc_prefixes = multiprocessing.Value('i',0)
total_prefixes = 0
processed = 0

def parse_arguments():
    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS

    print("\nEDGENET PREPROCESSOR\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset",      help="Directory (related to base_path), of the original data",  type=str)
    parser.add_argument("--base_path",  help="Base path of the original data. Default: "+BASE_PATH,     type=str, default=BASE_PATH,    required=False)
    parser.add_argument("--dest_path",  help="Destination path. Default: "+DEST_PATH,                   type=str, default=DEST_PATH,    required=False)
    parser.add_argument("--device",     help="CUDA device. Default " + str(DEVICE),                     type=int, default=DEVICE,       required=False)
    parser.add_argument("--threads",    help="Concurrent threads. Default " + str(THREADS),             type=int, default=THREADS,      required=False)
    args = parser.parse_args()

    BASE_PATH   = args.base_path
    DATASET     = args.dataset
    DEST_PATH   = args.dest_path
    DEVICE      = args.device
    THREADS     = args.threads

def process_multi(file_prefix):

    worker = multiprocessing.current_process()._identity[0]
    if worker <= 4:
        worker_device = 0
    else:
        worker_device = 1

    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS, to_process, proc_prefixes, total_time, processed

    if file_prefix not in [
        "/d02/data/csscnet/SUNCG/Val/7724bf6205df58dde671dc1c84e11b80/00000015",
        "/d02/data/csscnet/SUNCG/Train/4185b8d8c608d88f61b04d6c0967c87a/00000028",]:

        lib_sscnet_setup(device=worker_device, num_threads=128, K=None, frame_shape=(640, 480), v_unit=0.02, v_margin=0.24)

        dest_prefix = DEST_PATH + file_prefix[len(BASE_PATH):]
        # print("dest prefix:", dest_prefix)
        preproc_file = dest_prefix + '.npz'

        directory = os.path.split(dest_prefix)[0]
        # print("Directory", directory)

        with proc_prefixes.get_lock():
            if not os.path.exists(directory):
                os.makedirs(directory)

        shape = (240, 144, 240)
        down_shape=(shape[0]//4, shape[1]//4, shape[2]//4)

        #vox_tsdf, vox_rgb, segmentation_label, vox_weights = process(file_prefix, voxel_shape=shape, down_scale=4)
        vox_tsdf, vox_edges, tsdf_edges, segmentation_label, vox_weights, vox_vol = process(file_prefix,
                                                                                   voxel_shape=shape,
                                                                                   down_scale=4,
                                                                                   input_type=InputType.DEPTH_EDGES)
        if np.sum(segmentation_label) == 0:
            print(file_prefix)
            # exit(-1)
        else:
            np.savez_compressed(preproc_file,
                               tsdf=vox_tsdf.reshape((shape[0], shape[1], shape[2], 1)),
                               edges=tsdf_edges.reshape((shape[0], shape[1], shape[2], 1)),
                               lbl=segmentation_label.reshape((down_shape[0], down_shape[1], down_shape[2], 1)),
                               weights= vox_weights.reshape((down_shape[0], down_shape[1], down_shape[2])),
                               vol= vox_vol.reshape((down_shape[0], down_shape[1], down_shape[2])))

    with proc_prefixes.get_lock():
        proc_prefixes.value += 1

        counter = proc_prefixes.value
        mean_time = (time.time() - total_time)/counter
        eta_time = mean_time * (to_process - counter)
        eta_h = eta_time // (60*60)
        eta_m = (eta_time - (eta_h*60*60))//60
        eta_s = eta_time - (eta_h*60*60) - eta_m * 60
        perc = 100 * counter/to_process

        print("  %3.2f%%  Mean Processing Time: %.2f seconds ETA: %02d:%02d:%02d     " % (perc, mean_time, eta_h, eta_m, eta_s), end="\r")


# Main Function
def Run():
    global BASE_PATH, DATASET, DEST_PATH, DEVICE, THREADS, to_process, proc_prefixes, total_time
    parse_arguments()

    # find all processed files
    processed_files = get_file_prefixes_from_path(os.path.join(DEST_PATH, DATASET), criteria='*.npz')
    processed_files = [os.path.basename(x) for x in processed_files]
    print("Found %d processed files in destination path" % len(processed_files))

    # find all source files
    data_path = os.path.join(BASE_PATH, DATASET)
    print("Reading files from data path %s" % data_path)
    all_prefixes = get_file_prefixes_from_path(data_path)
    all_prefixes = [os.path.basename(x) for x in all_prefixes]

    # find not-yet processsed by comparing file_prefixes
    file_prefixes = [x for x in all_prefixes if x not in processed_files]
    file_prefixes = [os.path.join(BASE_PATH, DATASET, x) for x in file_prefixes]
    print("File prefixes:", file_prefixes)

    print("\nSUMMARY")
    print("Files: %d. Already processed: %d. To process: %d.\n" % (len(all_prefixes), len(processed_files), len(file_prefixes)))

    total_time = time.time()
    to_process = len(file_prefixes)
    if to_process > 0:
        pool = multiprocessing.Pool(processes=THREADS)
        pool.map(process_multi, file_prefixes)
        pool.close()
        pool.join()

    print("Total time: %s minutes      " % str((time.time() - total_time)//60))

if __name__ == '__main__':
    Run()
