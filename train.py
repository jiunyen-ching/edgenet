# Seed value
# seed_value = 7
# seed_value = 69
seed_value = 1234 # v1
# seed_value = 12345
# seed_value = 777

# Set the PYTHONHASHSEED environment variable at a fixed value
import os
os.environ['PYTHONHASHSEED']=str(seed_value)

# Set the python built-in pseudo-random generator at a fixed value
import random
random.seed(seed_value)

# Set the numpy pseudo-random generator at a fixed value
import numpy as np
np.random.seed(seed_value)

# Set the tensorflow pseudo-random generator at a fixed value
import tensorflow as tf
tf.random.set_seed(seed_value) # TF2.X
# tf.set_random_seed(seed_value) # TF1.X

# To run tf-decorated functions
tf.config.run_functions_eagerly(True) # TF2.X

import argparse

# default settings
BATCH_SIZE      = 3
BASE_LR         = 0.01
DECAY           = 0.0005
PATIENCE        = 4
WORKERS         = 6
GPU             = 1
WEIGHTS         = 'none'
NETWORK         = 'none'
DATASET         = 'none'
CYCLES          = 8.0
ONE_CYCLE       = False
BALANCE         = True
VAL_BATCH_SIZE  = 1
EPOCHS          = 30

def parse_arguments():
    global NETWORK, DATASET, BATCH_SIZE, BASE_LR, DECAY, WORKERS, PATIENCE, GPU, WEIGHTS, CYCLES, BALANCE, ONE_CYCLE, VAL_BATCH_SIZE, EPOCHS

    print("\nSemantic Scene Completion Training Script\n")
    parser = argparse.ArgumentParser()
    parser.add_argument("network",          help="CNN to be trainded", type=str, choices=[  'SSCNET',   'SSCNET_C', 'SSCNET_E',
                                                                                            'UNET',     'UNET_C',   'UNET_E',   'UNET_E_V2',
                                                                                            'R_UNET',               'R_UNET_E',
                                                                                            'DENSENET', 'DENSE_UNET',
                                                                                            'MBLLEN',   'R_UNET_PROJ'
                                                                                            ])
    parser.add_argument("dataset",          help="Base path of the original data",                          type=str,   choices=['SUNCG','NYU','NYUCAD','GEN'])
    parser.add_argument("--batch_size",     help="Training batch size. Default: " + str(BATCH_SIZE),        type=int,   default=BATCH_SIZE, required=False)
    parser.add_argument("--val_batch_size", help="Val batch size. Default: " + str(VAL_BATCH_SIZE),         type=int,   default=BATCH_SIZE, required=False)
    parser.add_argument("--base_lr",        help="CUDA device. Default " + str(BASE_LR),                    type=float, default=BASE_LR,    required=False)
    parser.add_argument("--decay",          help="Concurrent threads. Default " + str(DECAY),               type=float, default=DECAY,      required=False)
    parser.add_argument("--patience",       help="Epoch improvement patience. Default " + str(PATIENCE),    type=int,   default=PATIENCE,   required=False)
    parser.add_argument("--workers",        help="Concurrent threads. Default " + str(WORKERS),             type=int,   default=WORKERS,    required=False)
    parser.add_argument("--gpu",            help="GPU device. Default " + str(GPU),                         type=int,   default=GPU,        required=False)
    parser.add_argument("--weights",        help="Pretraind weights.",                                      type=str,   default=WEIGHTS,    required=False)
    parser.add_argument("--balance",        help="Use data balance?",                                       type=str,   default='yes',      required=False, choices=['YES', 'yes', 'Yes', 'Y', 'y', 'NO', 'no', 'No', 'N', 'n'])
    parser.add_argument("--cycles",         help="How many full cycles?",                                   type=float, default=CYCLES,     required=False)
    parser.add_argument("--epochs",         help="How many epochs?",                                        type=float, default=EPOCHS,     required=False)
    parser.add_argument("--one_cycle",      help="Uses one cycle lr?",                                      type=str,   default=ONE_CYCLE,  required=False, choices=['YES', 'yes', 'Yes', 'Y', 'y', 'NO', 'no', 'No', 'N', 'n'])
    args = parser.parse_args()

    NETWORK         = args.network
    DATASET         = args.dataset
    BATCH_SIZE      = args.batch_size
    VAL_BATCH_SIZE  = args.val_batch_size
    BASE_LR         = args.base_lr
    DECAY           = args.decay
    PATIENCE        = args.patience
    WORKERS         = args.workers
    GPU             = args.gpu
    WEIGHTS         = args.weights
    BALANCE         = args.balance in ['YES', 'yes', 'Yes', 'Y', 'y']
    CYCLES          = args.cycles
    ONE_CYCLE       = args.one_cycle in ['YES', 'yes', 'Yes', 'Y', 'y']
    EPOCHS          = args.epochs


def train():
    import os
    from lib_csscnet.path_config import read_config
    from train_utils.misc import get_runcount

    runcount = get_runcount() # for the purpose of naming weights

    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
    if GPU!=99:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(GPU)
        print("Use GPU number ", GPU)
    else:
        file_w = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'condor/condor_train_'+str(runcount)+'.out')
        fd = open(file_w, "a")
        fd.write("Use default GPU (condor assignment)\n")
        fd.flush()
        os.fsync(fd)

    os.environ["KERAS_BACKEND"] = "tensorflow"


    from lib_csscnet.file_utils import preproc_generator,get_file_prefixes_from_path
    from lib_csscnet.metrics import combined_iou # comp_iou, seg_iou
    from lib_csscnet.losses import weighted_categorical_crossentropy, set_batch_size, cat_crossent_nobalance
    from train_utils.callbacks import get_callbacks, CondorLogger

    set_batch_size(BATCH_SIZE)

    from tensorflow.keras.optimizers import SGD # TF2.X
    # from keras.optimizers import SGD # TF1.X

    from lib_csscnet.network import get_network_by_name

    model_name      = NETWORK + '_LR' + str(BASE_LR) + '_DC' + str(DECAY)
    model_name_run  = model_name + "_" + str(runcount)

    print("Training Model:", model_name_run)
    if GPU == 99:
        fd.write("Training Model: %s\n" % model_name_run)
        fd.flush()
        os.fsync(fd)

    voxel_shape = (240, 144, 240)

    train_path, val_path = read_config(DATASET)
    print("Getting file prefixes...")
    if GPU == 99:
        fd.write("Getting file prefixes...\n")
        fd.flush()
        os.fsync(fd)
    train_prefixes  = get_file_prefixes_from_path(train_path, criteria='*.npz')
    val_prefixes    = get_file_prefixes_from_path(val_path, criteria='*.npz')
    print("Train %s: Images: %d" % (train_path, len(train_prefixes)))
    print("Val %s: images: %d" % (val_path, len(val_prefixes)))
    if GPU == 99:
        fd.write("Train %s: Images: %d\n" % (train_path, len(train_prefixes)))
        fd.write("Val %s: images: %d\n" % (val_path, len(val_prefixes)))
        fd.flush()
        os.fsync(fd)

    print("Getting network %s" % NETWORK)
    if GPU == 99:
        fd.write("Getting network %s\n" % NETWORK)
        fd.flush()
        os.fsync(fd)

    model, _type = get_network_by_name(NETWORK)

    print("Seting up data generators...")
    if GPU == 99:
        fd.write("Seting up data generators...\n")
        fd.flush()
        os.fsync(fd)

    train_datagen = preproc_generator(train_prefixes, batch_size=BATCH_SIZE, shuff=True, aug=True,
                                      shape=voxel_shape, _type=_type)
    val_datagen = preproc_generator(val_prefixes, batch_size=VAL_BATCH_SIZE, shuff=False, aug=False,
                                    shape=voxel_shape, _type=_type)
    if BALANCE:
        loss = weighted_categorical_crossentropy
    else:
        loss = cat_crossent_nobalance

    print("Compiling model...")
    if GPU == 99:
        fd.write("Compiling model...\n")
        fd.flush()
        os.fsync(fd)

    # model.summary(line_length=150)
    model.compile(optimizer=SGD(learning_rate=BASE_LR, decay=DECAY, momentum=0.9),
                  loss=loss,
                  metrics=[combined_iou])

    log_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log')
    # weight_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'weights/'+WEIGHTS)
    weight_path = os.path.join('/content/drive/MyDrive/csscnet_preproc_edges/NYUedge', 'colab_weights/'+WEIGHTS)

    print("Log:",log_dir)
    print("Weight:",WEIGHTS)
    if GPU == 99:
        fd.write("Log: %s...\n" % log_dir)
        fd.write("Weights: %s...\n" % weight_path)
        fd.flush()
        os.fsync(fd)

    if WEIGHTS!='none':
        print("Pretrained weights:", WEIGHTS)
        model.load_weights(weight_path)

    print("Starting training...")
    if GPU == 99:
        fd.write("Starting training... %s - %d Epochs - %f Cycles\n" % (DATASET, EPOCHS,CYCLES))
        fd.flush()
        os.fsync(fd)

    if ONE_CYCLE:
        callbacks = get_callbacks(model_name_run, log_dir=log_dir, patience=PATIENCE, one_cycle_lr=BASE_LR)
    else:
        callbacks = get_callbacks(model_name_run, log_dir=log_dir, patience=PATIENCE, one_cycle_lr=None)

    if GPU==99:
        callbacks.append(CondorLogger(fd))

    model.fit(
        train_datagen,
        steps_per_epoch     =   np.ceil((len(train_prefixes) // BATCH_SIZE) * CYCLES/EPOCHS),
        # steps_per_epoch   =   3,
        epochs              =   EPOCHS,
        validation_data     =   val_datagen,
        validation_steps    =   np.ceil((len(val_prefixes) // VAL_BATCH_SIZE)),
        # validation_steps  =   5,
        callbacks           =   callbacks,
        workers             =   WORKERS
    )
    print("End of train...")
    if GPU == 99:
        fd.write("End of train...\n")
        fd.flush()
        os.fsync(fd)


if __name__ == '__main__':
    parse_arguments()
    train()
