import argparse

# default settings
BATCH_SIZE = 3
BASE_LR = 0.01
DECAY = 0.0005
PATIENCE = 4
WORKERS = 6
GPU = 0
WEIGHTS = 'none'
DATASET = 'none'

def parse_arguments():
    global NETWORK, DATASET, BATCH_SIZE, BASE_LR, DECAY, WORKERS, PATIENCE, GPU, WEIGHTS

    print("\nSemantic Scene Completion Evaluation Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("weights", help="Weigths to evaluate", type=str)
    parser.add_argument("dataset", help="Dataset to evaluate at", type=str, choices=['SUNCG','NYU'])
    parser.add_argument("--gpu", help="GPU device. Default " + str(GPU), type=int, default=GPU, required=False)
    args = parser.parse_args()

    WEIGHTS = args.weights
    DATASET = args.dataset
    GPU = args.gpu


def evaluate():
    VAL_BATCH_SIZE= 1

    from lib_csscnet.path_config import read_config

    import os
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = str(GPU)
    os.environ["KERAS_BACKEND"] = "tensorflow"

    from lib_csscnet.file_utils import preproc_generator, preproc_generator_v2, get_file_prefixes_from_path
    from lib_csscnet.metrics import comp_iou, seg_iou
    from lib_csscnet.losses import weighted_categorical_crossentropy
    from lib_csscnet.py_cuda import get_segmentation_class_map
    from lib_csscnet.metrics import comp_iou_np, seg_iou_np


    import numpy as np # linear algebra
    from keras.optimizers import SGD

    from lib_csscnet.network import get_network_by_name, get_net_name_from_w

    import h5py

    ##################################

    voxel_shape = (240, 144, 240)

    class_names = ["ceil.", "floor", "wall ", "wind.", "chair", "bed  ", "sofa ", "table", "tvs  ", "furn.", "objs."]

    ##################################

    print(DATASET)

    train_path, val_path, mat_path = read_config(DATASET, mat=True)
    val_prefixes = get_file_prefixes_from_path(val_path, criteria='*.npz')

    print("Eval %s: images: %d" % (val_path, len(val_prefixes)))


    NETWORK = get_net_name_from_w(WEIGHTS)

    print(WEIGHTS, NETWORK)
    model, type = get_network_by_name(NETWORK)

    if DATASET =='SUNCG':
        mat_pos = -60
    elif DATASET =='NYU':
        mat_pos = -12

    file_prefixes = get_file_prefixes_from_path(val_path, criteria='*.npz')

    eval_datagen = preproc_generator_v2(file_prefixes, batch_size=1, shuff=False, shape=voxel_shape, type=type)


    model.compile(optimizer=SGD(lr=BASE_LR, decay=DECAY,  momentum=0.9),
                  loss=weighted_categorical_crossentropy,
                  metrics=[comp_iou, seg_iou])


    model_name = os.path.join('./weights',WEIGHTS)

    model.load_weights(model_name)

    print ("\nEvaluating model", WEIGHTS, "on %s. %d views." % (DATASET, len(file_prefixes)))

    comp_inter = 0
    comp_union = 0
    comp_tp = 0
    comp_fp = 0
    comp_fn = 0

    seg_inter = np.zeros((11,), dtype='float32')
    seg_union = np.zeros((11,), dtype='float32')

    #max = int(np.ceil(len(file_prefixes)/BATCH_SIZE))

    segmentation_class_map = get_segmentation_class_map()

    for round in range(len(file_prefixes)):

        mat_prefix = file_prefixes[round][mat_pos:]
        print(mat_prefix, end="  ")

        #f = h5py.File(mat_path+'/' + mat_prefix + '_gt_d4.mat', 'r')
        #scene_vox = np.array(f['sceneVox_ds'])
        #scene_vox[scene_vox==255] = 0;
        #scene_vox[np.isnan(scene_vox)] = 0;
        #label_obj = np.vectorize(lambda x: segmentation_class_map[int(x)])(scene_vox)

        f = h5py.File(mat_path+'/' + mat_prefix + '_vol_d4.mat', 'r')
        vol = np.array(f['flipVol_ds'])

        x, y = next(eval_datagen)

        pred = model.predict(x=x)
        pred_classes = np.argmax(pred, axis=-1)

        inter, union, tp, fp, fn = comp_iou_np(y, pred, vol)
        comp_inter += inter
        comp_union += union
        comp_tp += tp
        comp_fp += fp
        comp_fn += fn

        for cl in range(0,11):
            inter, union = seg_iou_np(y, pred, vol, cl+1)
            seg_inter[cl] += inter
            seg_union[cl] += union

        if (comp_tp + comp_fp) > 0:
            precision = comp_tp / (comp_tp + comp_fp)
        else:
            precision = 0

        if (comp_tp + comp_fn) > 0:
            recall = comp_tp / (comp_tp + comp_fn)
        else:
            recall = 0

        print("%d/%d Partial    Precision: %.1f  Recall: %.1f   Comp IOU: %.1f   Seg IOU: %.1f "
              % (round,len(file_prefixes),
                 precision *100, recall * 100,
                 100 * comp_inter / comp_union,
                 100*np.mean((seg_inter+0.00001)/(seg_union+0.00001))), end='\r')



    print("\nCompletion IOU: %.1f\n" %(100*comp_inter/comp_union))

    for cl in range(0, 11):
        print("Class %s \tSegmentation IOU: %.1f" %(class_names[cl], 100 * seg_inter[cl] / seg_union[cl]))

    print("\nMean Segmentation IOU: %.1f" %(100*np.mean(seg_inter/seg_union)))


# Main Function
def Run():
    parse_arguments()
    evaluate()


if __name__ == '__main__':
  Run()
  