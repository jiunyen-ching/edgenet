import os
from tensorflow.keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard,LearningRateScheduler # TF2.X
import tensorflow.keras.backend as K # TF2.X
# from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard,LearningRateScheduler # TF1.X
# import keras.backend as K # TF1.X

class CondorLogger(Callback):
    """
    A Logger for condor.
    """
    def __init__(self, fd):
        self.step = 0
        self.fd= fd

    def on_batch_end(self, batch, logs={}):
        self.step += 1
        if self.step % 10 == 1:
            self.fd.write("%4d:%.4f " % (self.step, logs['combined_iou']))
            self.fd.flush()
            os.fsync(self.fd)

        if self.step % 100 == 0:
            self.fd.write("\n")
            self.fd.flush()
            os.fsync(self.fd)

    def on_epoch_end(self, epoch, logs={}):
        self.fd.write("\n\nEpoch: %d  - Val Combined IOU: %.4f\n\n" % (epoch+1, logs['val_combined_iou']))
        self.fd.flush()
        os.fsync(self.fd)
        self.step = 0

class OneCycleLR:
    def __init__(self, base_lr = 0.01, max_lr = 0.1, min_lr = 0.00001, point1 = 10, point2 = 20, point3 = 25):
        self.point1 = point1
        self.point2 = point2
        self.point3 = point3
        self.base_lr = base_lr
        self.max_lr = max_lr
        self.min_lr = min_lr

    def get_lr(self, iteration):
        """Given the inputs, calculates the lr that should be applicable for this iteration"""
        if iteration < self.point1:
            lr = self.base_lr + iteration*(self.max_lr-self.base_lr)/self.point1
        elif iteration < self.point2:
            lr = self.max_lr - (iteration-self.point1)*(self.max_lr-self.base_lr)/(self.point2-self.point1)
        elif iteration < self.point3:
            lr = self.base_lr - (iteration-self.point2)*(self.base_lr-self.min_lr)/(self.point3-self.point2)
        else:
            lr = self.min_lr

        return lr


class log_lr(Callback):
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['lr'] = K.get_value(self.model.optimizer.lr)


def get_callbacks(model_name, log_dir="/tmp/tensor_board", patience=30, one_cycle_lr=None):
    es = EarlyStopping(patience=patience, mode="max", monitor="combined_iou")
    msave = ModelCheckpoint(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'weights/'+model_name+'{epoch:02d}-{combined_iou:.2f}-{val_combined_iou:.2f}.hdf5'),
                            save_best_only=False, save_weights_only=True, mode="max", monitor="combined_iou")
    log = TensorBoard(log_dir=log_dir+"/"+model_name)
    cb = [es, msave, log, log_lr()]
    if one_cycle_lr is not None:
        oclr = OneCycleLR(one_cycle_lr)
        lre = LearningRateScheduler(oclr.get_lr)
        cb.append(lre)


    #red = ReduceLROnPlateau(patience=patience//5, verbose=1, cooldown=patience//6)

    return cb

