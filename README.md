# EdgeNet

### Semantic Scene Completion from RGB-D images

![teaser](teaser.png)

## Contents
0. [Organization](#organization)
0. [Requirements](#requirements)
0. [Compiling](#compiling)
0. [Download Datasets](#downloaddatasets )
0. [Preprocessing](#preprocessing)
0. [Training](#training)
0. [Evaluating](#evaluating)
0. [Rendering](#rendering)

## Organization 
The project folder is organized as follows:
``` shell
    EdgeNet
         |-- blender               //blender rendering scripts and sample voxel volumes
         |-- lib_sscnet            //py-cuda integration and other EdgeNet python libraries
         |-- src                   //cuda source code  (preprocessing)
         |-- train_utils           //auxiliary python libraries for training
```

## Requirements 
1. Hardware Requirements (minimum):

   * Ubuntu 16.04 LTS or newer
   * For training: a 11G GPU (for example, a GTX 1080TI) in order to train EdgeNet with a batch-size of 3 scenes
   * For inference only: a 4G GPU (for example, a GTX 1050TI) 

2. Software Requirements:

All software listed here are open-source.
 
   * GPU Drivers
   * Python 3
   * Anaconda 3: We recomend using an [Anaconda](https://www.anaconda.com/distribution/) virtual environment 
     for simplicity. 
   * Tensorflow with GPU/CUDA support. We suggest creating and activating a Anaconda virtual environment for 
     TensorFlow with GPU support using [these instructions](https://www.anaconda.com/tensorflow-in-anaconda/).
     tensorflow-gpu ver (tested on 1.12.0, 1.13.1)
   * [Keras](https://anaconda.org/conda-forge/keras).
     keras 2.2.4
   * [OpenCV 3 for python](https://anaconda.org/anaconda/py-opencv).
   * Python common libraries ([numpy](https://anaconda.org/anaconda/numpy), 
     [pandas](https://anaconda.org/anaconda/pandas) and other common packages according to your environment...) 
   * Blender (only required for rendering - install using you preferred package manager)
     blender2.79

## Compiling
To compile the cuda preprocessing shared library: 
``` shell
    cd src
    nvcc --ptxas-options=-v --compiler-options '-fPIC' -o lib_csscnet.so --shared lib_csscnet.cu
```
         
## Download Datasets
1. Download main data (1.1 GB) and SUNCG training set  (16 GB) following 
   instructions in [SSCNET git hub main page](https://github.com/shurans/sscnet).
2. Download SUNCG scene data and toolbox and compile the toolbox following instructions from: 
   [SUNCG git hub main page](https://github.com/shurans/SUNCGtoolbox) 
3. Download NYU V2 labeled dataset (2.8 GB)from [NYU V2 home page](https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html)

## Preprocessing

### Extract RGB images from SUNCG dataset
To extract RGB images of the scenes of SUNCG train and test sets from SUNCGv1 scene database, 
use the script **suncg_rgb_extract.py**. 

For each set you want to extract, first adjust the path variables in the script according to you environment:

``` shell
BASE_PATH = '/(...)/data/depthbin/SUNCGtest_49700_49884/'
TEMP_DIR = '/(...)/output'
suncgDataPath = '/(...)/data/suncg_v1/'
suncgToolboxPath = '/(...)/SUNCGtoolbox/'
```
Then, execute the script: 

``` shell
    python suncg_rgb_extract.py
```

RGB files for each scene will be extracted according camera parameters of original train or test set and placed 
beside corresponding depth file. 


### Extract RGB images from NYU v2 dataset
To extract RGB images of the scenes of NYU v2 Matlab file use the script **get_image_from_mat.py**. Matlab sofware 
is not required. 

For extraction, first adjust the path variables in the script according to you environment:

``` shell
MAT_FILE   = '/(...)/nyu-v2/nyu_depth_v2_labeled.mat'
TRAIN_PATH = '/(...)/NYU/NYUtrain'
VAL_PATH   = '/(...)/NYU/NYUtest'
```

Then, execute the script: 

``` shell
    python get_image_from_mat.py
```

RGB files for each scene will be extracted from matlab file and placed beside corresponding depth file. 

You will also need to rename NYU depth files to include the **"_depth"** sufix, using the script **rename_nyu_depth.py**.

First adjust the path variables in the script according to you environment:

``` shell
TRAIN_PATH = '/(...)/NYU/NYUtrain'
VAL_PATH   = '/(...)/NYU/NYUtest'
```

Then, execute the script: 

``` shell
    python rename_nyu_depth.py
```

### Preprocess: F-TSDF encoding from Depth and Edges 


To preprocess a dataset  use the script **preproc_edges.py**. 

You may adjust path variables in the script according to you environment,
if you want. They also may be provided via command line parameters:

``` shell
BASE_PATH = '/(...)/sscnet/data/depthbin'
DATASET = 'SUNCGtest_49700_49884'
DEST_PATH = '/(...)/EdgeNet_preproc'
```

#### 1. Getting help:

``` shell
python preproc_edges.py -h
```

#### 2. Preprocessing SUNCG Test Set:

``` shell
python preproc_edges.py SUNCGtest_49700_49884 --base_path /(...)/sscnet/data --dest_path /(...)/EdgeNet_preproc
```
#### 3. Preprocessing NYU Train Set:

``` shell
python preproc_edges.py SUNCGtrain --base_path /(...)/sscnet/data --dest_path /(...)/EdgeNet_preproc
```

## Path Configuration for Training and Evaluation

File paths.conf holds datasets paths definitions for simplifying training and evaluation.

You may adjust it according your environment.

## Training 

To train or fine tuning a network you use the script **train.py**.
File **paths.conf** should be correctly configured to reflect your environment.

Network codes vs Network Names:
* SSCNET: SSCNet
* SSCNET_E: E-SSCNet
* R_UNET: U-SSCNet
* R_UNET_E: EdgeNet

Trained models (.hdf5 weight files) are stored on ./weights directory.
Do not change the name of the generated weight files, as it contains the code of the network.
You can monitor the training processes using Tensorboard. Tensorboard log directory is ./log
Each train run is given a name which contains a sequencial number at the end.   

### 1. Getting help:

``` shell
python train.py -h
```
#### 2. Training EdgeNet on SUNCG from scratch using GPU 1, 8 scenes per batch (Requires a 24GB GPU):

``` shell
python train.py --base_lr 0.01 --patience 10  --gpu 1 --batch_size 8  --val_batch_size 2 --one_cycle yes --cycles 8.0 --epochs 30 R_UNET_E SUNCG
```

#### 3. Training SSCNet on NYU from scratch using default GPU, 4 scenes per batch (Requires a 11GB GPU):

``` shell
python train.py --base_lr 0.01 --patience 10  --gpu 99 --batch_size 4  --val_batch_size 2 --one_cycle yes --cycles 30.0 --epochs 30 SSCNET NYU
```

#### 4. Fine Tuning a pretrained EdgeNet model using GPU 0, 3 scenes per batch (Requires a 11GB GPU):

``` shell
python train.py --weights (weight_file.hdf5)  --base_lr 0.01 --patience 10  --gpu 0 --batch_size 3  --val_batch_size 2 --one_cycle no --cycles 30.0 --epochs 30 R_UNET_E NYU
```

## Evaluating 

To evaluate a trained model you use the script **evaluate.py**. 
File **paths.conf** should be correctly configured to reflect your environment.


  
### 1. Getting help:

``` shell
python evaluate.py -h
```
#### 2. Evaluating a trained model on SUNCG:

``` shell
python evaluate.py (weight_file.hdf5) SUNCG
```
#### 3. Evaluating a trained model on NYU:

``` shell
python evaluate.py (weight_file.hdf5) NYU
```

## Rendering 

To render a scene GT or a model prediction for a scene, you first need to generate the voxels volumes,
then you use Blender to render the images.

Use the script **volume_generator.py** as a template to generate your own volumes. You need to setup
a number of path variables and choices inside the script. Follow the comments in the code.
Volumes are .bin files and are be placed in the ./blender directory.

After generating the volumes, call Blender from a terminal:
*Note: Works with Blender 2.79 and not 2.80 yet

``` shell
cd blender
blender
```

Open the provided example blender file (volume_render0.blend)
In the script selection drop box (botton-left of Blender window) 
select one of the render scripts (gt_render or Prediction_render). Locate this line:

``` shell
f = open("suncg_R_UNET_E0.bin", 'rb')
```

Change file name to meet your settings.

Run the script. In some versions of Blender, it may be necessary to clean the view before loading another volume.
You can monitor the loading process on the terminal that you opened Blender. All script errors are also shown on 
that terminal. 

After loading the volume, you can press the render button to render the image. You can also change, zoom, 
position, perspective, as you want. 

## Running on Google Colab

On Tesla T4, you need:
   * CUDA: 10.0
   * cuDNN 7.5.1.10
   * Tensorflow-GPU 1.13.1
   * Keras 2.2.4
   
## Additional libraries if a fresh conda env is used
### 1. pandas
### 2. OpenCV
### 3. Scikit-learn
### 4. Pillow



